package com.example.netbank.Controller;

import com.example.netbank.Entity.*;
import com.example.netbank.Service.AccountService;
import com.example.netbank.Service.TotpManager;
import com.example.netbank.Service.UserService;
import com.example.netbank.captcha.ICaptchaService;
import com.example.netbank.captcha.ReCaptchaInvalidException;
import com.example.netbank.captcha.ReCaptchaUnavailableException;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class LoginController {
	private final UserService userService;
	private final TotpManager totpManager;
	private final UserController userController;
	@Resource(name = "authenticationManager")
	private AuthenticationManager authManager;
	private final ICaptchaService captchaService;
	
	@RequestMapping(value = {"/login"}, method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}
	
	@RequestMapping(value = {"/loginConfirm"}, method = RequestMethod.POST)
	public ModelAndView loginPostProcess() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}
	
	@RequestMapping(value = {"/login"}, method = RequestMethod.POST)
	public ModelAndView loginPost(@RequestParam("user_name") final String username, @RequestParam("password") final String password, final HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		User user = userService.findByUsername(username).orElseThrow(() -> new RuntimeException("Invalid username"));
		if (user.isMfa()) {
			modelAndView.addObject("mfa", new MfaLogIn(user.getUserName(), password, ""));
			modelAndView.setViewName("verify");
			return modelAndView;
		}
		UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(username, password);
		Authentication auth = authManager.authenticate(authReq);
		
		SecurityContext sc = SecurityContextHolder.getContext();
		sc.setAuthentication(auth);
		HttpSession session = request.getSession(true);
		session.setAttribute("SPRING_SECURITY_CONTEXT", sc);
		return userController.home();
	}
	
	
	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView registration() {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("registration");
		return modelAndView;
	}
	

	
	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	public String verify(MfaLogIn mfaLogIn, BindingResult bindingResult, final HttpServletRequest request) {
		User user = userService.findByUsername(mfaLogIn.getUsername()).orElse(null);
		if (user == null) {
			throw new RuntimeException("Username is incorrect");
		}
		
		if (!totpManager.verifyCode(mfaLogIn.getCode(), user.getSecret())) {
			throw new RuntimeException("Code is incorrect");
		}
		UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(mfaLogIn.getUsername(), mfaLogIn.getPassword());
		Authentication auth = authManager.authenticate(authReq);
		
		SecurityContext sc = SecurityContextHolder.getContext();
		sc.setAuthentication(auth);
		HttpSession session = request.getSession(true);
		session.setAttribute("SPRING_SECURITY_CONTEXT", sc);
		return "redirect:/user/home";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult, @RequestParam("g-recaptcha-response") String request) {
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByUserName(user.getUserName());
		//TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO MÁTÉÉÉÉÉÉÉ
		try {
			captchaService.processResponse(request);
		} catch (ReCaptchaInvalidException | ReCaptchaUnavailableException e) {
			bindingResult
				.rejectValue("userName", "error.user",
					"There is captcha problem");
			return modelAndView;
		}
		if (userExists != null) {
			bindingResult
				.rejectValue("userName", "error.user",
					"There is already a user registered with the user name provided");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
		} else {
			user.setSecret(totpManager.generateSecret());
			User saveUser = userService.saveUser(user);
			if (user.isMfa()) {
				modelAndView.addObject("mfa", new SignupResponse(saveUser.isMfa(), totpManager.getUriForImage(saveUser.getSecret())));
				modelAndView.setViewName("qrcode");
				return modelAndView;
			}
			modelAndView.addObject("successMessage", "User has been registered successfully");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("registration");
			
		}
		return modelAndView;
	}
	
}
