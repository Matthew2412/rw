package com.example.netbank.Controller;

import com.example.netbank.Entity.*;
import com.example.netbank.Service.AccountService;
import com.example.netbank.Service.CardService;
import com.example.netbank.Service.TransactionService;
import com.example.netbank.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    AccountService accountService;
    @Autowired
    CardService cardService;
    @Autowired
    TransactionService transactionService;

    @RequestMapping(value = "/balance" ,method = RequestMethod.POST)
    public String uploadmoney(@ModelAttribute(value="balance") @RequestParam("card")Long id, Integer balance){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUserName(auth.getName());
        Account byId = accountService.findById(user.getId());
        Card cardByID = cardService.getByID(id).get();
        cardByID.setBalance(cardByID.getBalance()+balance);
        byId.setBalance(byId.getBalance()+balance);
        Transaction transaction = new Transaction();
        transaction.setAccountFromId(byId);
        transaction.setAmount(balance);
        transaction.setTransactionType(TransactionType.UPLOAD);
        transaction.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transactionService.save(transaction);
        accountService.save(byId);
        cardService.save(cardByID);
        return "redirect:/user/balance";
    }
    @RequestMapping(value = "/balance" ,method = RequestMethod.GET)
    public ModelAndView showBalance(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUserName(auth.getName());
        List<Account> byUser = accountService.findByUser(user);
        List<Card> cards = new ArrayList<>();
        for (Account account: byUser
             ) {
            cards.addAll(cardService.getAllCardsViaAccountObject(account));
        }
        Account byId = accountService.findById(user.getId());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user/balance");
        modelAndView.addObject("cardlist",cards );
        Integer amount =0;
        modelAndView.addObject("amount",amount);
        return modelAndView;
    }
    @RequestMapping(value="/home", method = RequestMethod.GET)
    public ModelAndView home(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUserName(auth.getName());
        List<Transaction> allTransactions = new ArrayList<>();

        for (Account account: accountService.findByUser(user)){
                allTransactions.addAll(account.transactionList);
                //allTransactions.addAll(account.transactionList1);
            // transactionService.findAllTransactionByAccountToId(account);
            // allTransactions.addAll(transactionService.findAllTransactionByAccountFromId(account));
            //  allTransactions.addAll(transactionService.findAllTransactionByAccountToId(account));
        }
        //List<Transaction> sortedTranscations = allTransactions.stream().sorted(Comparator.comparing(Transaction::getTimestamp)).collect(Collectors.toList());

        modelAndView.addObject("sortedTranscations",allTransactions);
        modelAndView.addObject("accounts",accountService.findByUser(user));
        modelAndView.addObject("userName", "User name: " + " " + user.getUserName());
        modelAndView.addObject("userdetailedName",  user.getUserName() + "/" + user.getName() + " " + user.getLastName());
        modelAndView.addObject("adminMessage","Content Available Only for Users with Admin Role");
        modelAndView.setViewName("user/menutest");
        return modelAndView;
    }
    @RequestMapping(value = "/createCreditCard" ,method = RequestMethod.GET)
    public ModelAndView showCRUDCreditCard(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUserName(auth.getName());
        List<Account> byId = accountService.findByUser(user);
        modelAndView.setViewName("user/createCreditCard");
        modelAndView.addObject("accountList ", byId);
            modelAndView.addObject("listofaccounts",byId);
        return modelAndView;
    }
    @RequestMapping(value = "/createCreditCard/{id}" ,method = RequestMethod.POST)
    public String cardCreated(@PathVariable("id") long accountId){
        Account byId =accountService.findById(accountId);
        Card card = new Card();
        card.setAccount(byId);
        card.setExpirationDate(Timestamp.valueOf(LocalDateTime.now().plusMonths(12)));
        cardService.save(card);
        byId.setBalance(byId.getBalance()- TransactionType.CREDIT_CARD_FEE.amount);
        Transaction transaction = new Transaction();
        transaction.setAccountFromId(byId);
        transaction.setAmount(-TransactionType.CREDIT_CARD_FEE.amount);
        transaction.setTransactionType(TransactionType.CREDIT_CARD_FEE);
        transaction.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transactionService.save(transaction);
        List<Transaction> transactionList = byId.getTransactionList();
        transactionList.add(transaction);
        accountService.save(byId);
        return "redirect:/user/createCreditCard";
    }
    @RequestMapping(value = "/delete?id={cardId}" , method = RequestMethod.GET)
    public String deleteBuyer(@PathVariable("cardId")long cardId) {
        cardService.deleteById(cardId);
        return "redirect:/";
    }
    @RequestMapping(value = "/delete/{id}" , method = RequestMethod.POST)
    public String deleteBuyer1(@PathVariable("id") Long cardId) {
        cardService.deleteById(cardId);
        return "redirect:/user/createCreditCard";
    }
    @RequestMapping(value = "/createtransaction", method = RequestMethod.GET)
    public ModelAndView showTransactionPage(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUserName(auth.getName());
        List<Account> byId = accountService.findByUser(user);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user/transactionPage");
        modelAndView.addObject("accounts",byId);
        return modelAndView;
    }
    @RequestMapping(value = "/createtransaction", method = RequestMethod.POST)
    public String doTransaction(@RequestParam("account") Long id, @RequestParam("toAccount") String toAccount , @RequestParam("amount") int amount){
        Account fromId = this.accountService.findById(id);
        fromId.setBalance(fromId.getBalance()-amount);
        Account toId = this.accountService.findAccountByAccountNumber(toAccount);
        toId.setBalance(toId.getBalance()+amount);
        Transaction transaction = new Transaction();
        transaction.setAccountFromId(fromId);
        transaction.setAccountToId(toId);
        transaction.setAmount(-amount);
        transaction.setTransactionType(TransactionType.TRANSFER);
        transaction.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transactionService.save(transaction);
        Transaction transaction1 = new Transaction();
        transaction1.setAccountFromId(fromId);
        transaction1.setAmount(-TransactionType.TRANSACTIONAL_FEE.amount);
        transaction1.setTransactionType(TransactionType.TRANSACTIONAL_FEE);
        transaction1.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transactionService.save(transaction1);
        accountService.save(fromId);
        accountService.save(toId);
        return "redirect:/user/home";
    }
    @RequestMapping(value = "/manageaccounts", method = RequestMethod.GET)
    public ModelAndView manageAccounts(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUserName(auth.getName());
        modelAndView.addObject("accounts",this.accountService.findByUser(user));
        modelAndView.setViewName("user/manageaccounts");
        return modelAndView;
    }
    @RequestMapping(value = "/manageaccounts", method = RequestMethod.POST)
    public String addAccounts(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByUserName(auth.getName());
        List<Account> accounts = user.getAccount();
        Account account = new Account();
        accounts.add(account);
        user.setAccount(accounts);
        userService.saveUser(user);
        accountService.save(account);
        return "redirect:/user/home";
    }
    @RequestMapping(value = "/deleteaccount/{id}" , method = RequestMethod.POST)
    public String deleteAccount(@PathVariable("id") Long accountId) {
        Account byId = accountService.findById(accountId);
        accountService.delete(byId);
        return "redirect:/user/home";
    }
    @RequestMapping(value = "transactionHistory/{id}",method = RequestMethod.GET)
    public ModelAndView showTransactionHistory(@PathVariable("id") Long accountId){
        ModelAndView modelAndView = new ModelAndView();
        Account byId = accountService.findById(accountId);
        modelAndView.addObject("fromList",transactionService.findAllTransactionByAccountFromId(byId));
        modelAndView.setViewName("user/transactionhistory");
        return modelAndView;

    }
}
