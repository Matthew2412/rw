package com.example.netbank.captcha;

import java.io.IOException;
import java.util.Properties;


import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;


import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.web.filter.OncePerRequestFilter;

public class CaptchaVerifierFilter extends OncePerRequestFilter {

    private Boolean useProxy = false;
    private String proxyPort;
    private String proxyHost;
    private String failureUrl;
    private CaptchaCaptureFilter captchaCaptureFilter;
    private String privateKey;

    private SimpleUrlAuthenticationFailureHandler failureHandler = new SimpleUrlAuthenticationFailureHandler();

    @Override
    public void doFilterInternal(HttpServletRequest req, HttpServletResponse res,
                                 FilterChain chain) throws IOException, ServletException {

        logger.debug("Captcha verifier filter");
        logger.debug("challenge: " + captchaCaptureFilter.getRecaptcha_challenge());
        logger.debug("response: " + captchaCaptureFilter.getRecaptcha_response());
        logger.debug("remoteAddr: " + captchaCaptureFilter.getRemoteAddr());

        if (captchaCaptureFilter.getRecaptcha_response() != null) {

            
            ReCaptchaImpl reCaptcha = new ReCaptchaImpl();

            reCaptcha.setPrivateKey(privateKey);

            if (useProxy) {
                Properties systemSettings = System.getProperties();
                systemSettings.put("http.proxyPort",proxyPort);
                systemSettings.put("http.proxyHost",proxyHost);
            }

            ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(captchaCaptureFilter.getRemoteAddr(), captchaCaptureFilter.getRecaptcha_challenge(), captchaCaptureFilter.getRecaptcha_response());

            if (!reCaptchaResponse.isValid()) {
                logger.debug("Captcha is invalid!");

                failureHandler.setDefaultFailureUrl(failureUrl);
                failureHandler.onAuthenticationFailure(req, res, new BadCredentialsException("Captcha invalid!"));

            } else {
                logger.debug("Captcha is valid!");
            }

            resetCaptchaFields();
        }

        chain.doFilter(req, res);
    }


    public void resetCaptchaFields() {
        captchaCaptureFilter.setRemoteAddr(null);
        captchaCaptureFilter.setRecaptcha_challenge(null);
        captchaCaptureFilter.setRecaptcha_response(null);
    }

    public Boolean getUseProxy() {
        return useProxy;
    }

    public void setUseProxy(Boolean useProxy) {
        this.useProxy = useProxy;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public String getFailureUrl() {
        return failureUrl;
    }

    public void setFailureUrl(String failureUrl) {
        this.failureUrl = failureUrl;
    }

    public CaptchaCaptureFilter getCaptchaCaptureFilter() {
        return captchaCaptureFilter;
    }

    public void setCaptchaCaptureFilter(CaptchaCaptureFilter captchaCaptureFilter) {
        this.captchaCaptureFilter = captchaCaptureFilter;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}