package com.example.netbank.captcha;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestOperations;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

public abstract class AbstractCaptchaService implements ICaptchaService{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AbstractCaptchaService.class);
	
	@Autowired
	protected CaptchaSettings captchaSettings;
	
	@Autowired
	protected ReCaptchaAttemptService reCaptchaAttemptService;
	
	@Autowired
	protected RestOperations restTemplate;
	
	protected static final Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");
	
	protected static final String RECAPTCHA_URL_TEMPLATE = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s";
	
	@Override
	public String getReCaptchaSite() {
		return captchaSettings.getSite();
	}
	
	@Override
	public String getReCaptchaSecret() {
		return captchaSettings.getSecret();
	}
	
	
	protected void securityCheck(final String response) {
		LOGGER.debug("Attempting to validate response {}", response);
		
		if (reCaptchaAttemptService.isBlocked(getClientIP())) {
			throw new ReCaptchaInvalidException("Client exceeded maximum number of failed attempts");
		}
		
		if (!responseSanityCheck(response)) {
			throw new ReCaptchaInvalidException("Response contains invalid characters");
		}
	}
	
	protected boolean responseSanityCheck(final String response) {
		return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
	}
	
	protected String getClientIP() {
		HttpServletRequest curRequest =
			((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		final String xfHeader = curRequest.getHeader("X-Forwarded-For");
		if (xfHeader == null) {
			return curRequest.getRemoteAddr();
		}
		return xfHeader.split(",")[0];
	}
}
