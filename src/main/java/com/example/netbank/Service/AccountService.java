package com.example.netbank.Service;

import com.example.netbank.Entity.Account;
import com.example.netbank.Entity.User;
import com.example.netbank.Repository.AccountRepository;
import com.example.netbank.Repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    public AccountService(AccountRepository accountRepository, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }
    public Account findById(Long id){
        return accountRepository.getOne(id);
    }
    public Account findAccountByAccountNumber(String s){
        return this.accountRepository.findAccountByAccountNumber(s);
    }
    public Account accountBalanceIncrease(int number, Account account){
        account.setBalance(account.getBalance()+number);
        return account;
    }
    public List<Account> findByUser(User user){
        return this.accountRepository.findByUser(user);
    }

    public boolean save(Account account) {
        accountRepository.save(account);
        return true;
    }
    public boolean delete(Account account){
        this.accountRepository.delete(account);
        return true;
    }

}
