package com.example.netbank.Service;

import com.example.netbank.Entity.Account;
import com.example.netbank.Entity.Role;
import com.example.netbank.Entity.User;
import com.example.netbank.Repository.AccountRepository;
import com.example.netbank.Repository.RoleRepository;
import com.example.netbank.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {
    private final UserRepository userRepository;
    @Autowired
    private  BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleRepository roleRepository;
    private final AccountRepository accountRepository;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, RoleRepository roleRepository, AccountRepository accountRepository) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleRepository = roleRepository;
        this.accountRepository = accountRepository;
    }
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User findUserByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }
    public User findUserByAccountNumber(String accountNumber){

        Account account = accountRepository.findAccountByAccountNumber(accountNumber);
        return userRepository.findUserByAccount(account);
    }

    public User saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(true);
        Role userRole = roleRepository.findByRole("ADMIN");
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        Account account = new Account();
        account.setUser(user);
        accountRepository.save(account);
        List<Account> accountList= new ArrayList<>();
        accountList.add(account);
        user.setAccount(accountList);
        return userRepository.save(user);
    }
    
    public Optional<User> findByUsername(String username) {
        return Optional.of(this.userRepository.findByUserName(username));
    }
}
