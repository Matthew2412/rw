package com.example.netbank.Service;

import com.example.netbank.Entity.Role;
import com.example.netbank.Entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MyUserDetailsService implements UserDetailsService {
    private final UserService userService;

    public MyUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userService.findUserByUserName(s);
        List<GrantedAuthority> grantedAuthorityList = getUserAuthority(user.getRoles());
        return buildUserForAuthentication(user, grantedAuthorityList);
    }

    private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> grantedAuthorityList) {
        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
                user.getActive(), true, true, true, grantedAuthorityList);
    }


    private List<GrantedAuthority> getUserAuthority(Set<Role> uroles) {
        Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
        for (Role role : uroles) {
            roles.add(new SimpleGrantedAuthority(role.getRole()));
        }
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles);
        return grantedAuthorities;
    }
}