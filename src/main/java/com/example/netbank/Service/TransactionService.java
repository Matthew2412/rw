package com.example.netbank.Service;

import com.example.netbank.Entity.Account;
import com.example.netbank.Entity.Transaction;
import com.example.netbank.Repository.TransacitionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {
    private final TransacitionRepository transacitionRepository;

    public TransactionService(TransacitionRepository transacitionRepository) {
        this.transacitionRepository = transacitionRepository;
    }
    public boolean save(Transaction t){
        this.transacitionRepository.save(t);
        return true;
    }
    public List<Transaction> findAllTransactionByAccountFromId(Account account){
        return this.transacitionRepository.findByAccountFromId(account);
    }
    public List<Transaction> findAllTransactionByAccountToId(Account account){
        return this.transacitionRepository.findByAccountToId(account);
    }
}
