package com.example.netbank.Service;

import com.example.netbank.Entity.Account;
import com.example.netbank.Entity.Card;
import com.example.netbank.Repository.AccountRepository;
import com.example.netbank.Repository.CardRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CardService {
    private final CardRepository cardRepository;
    private final AccountRepository accountRepository;

    public CardService(CardRepository cardRepository, AccountRepository accountRepository) {
        this.cardRepository = cardRepository;
        this.accountRepository = accountRepository;
    }
    public List<Card> getAllCardsViaAccountNumber(String accountNumber){
        Account accountByAccountNumber = accountRepository.findAccountByAccountNumber(accountNumber);
        return cardRepository.findByAccount(accountByAccountNumber);
    }
    public List<Card> getAllCardsViaAccountObject(Account account){
        return cardRepository.findByAccount(account);
    }
    public boolean save(Card card){
        this.cardRepository.save(card);
        return true;
    }
    public Optional<Card> getByID(Long id){
        return this.cardRepository.findById(id);
    }
    public void deleteById(Long id){
        this.cardRepository.deleteById(id);
    }
}
