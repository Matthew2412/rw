package com.example.netbank.Service;

import com.example.netbank.Entity.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getAllEmployees();
}
