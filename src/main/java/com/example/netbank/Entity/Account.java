package com.example.netbank.Entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Entity
@Audited
@Builder
@Data
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "users_id")
    @ToString.Exclude
    private User user;
    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;
    @Column(name = "BALANCE")
    private Integer Balance =10;

    @OneToMany(mappedBy = "account",cascade = CascadeType.ALL)
    @ToString.Exclude
    public List<Card> cardList;
    @OneToMany(mappedBy = "accountFromId",cascade = CascadeType.ALL)
    @ToString.Exclude
    public List<Transaction> transactionList = new ArrayList<>();
    @ToString.Exclude
    @OneToMany(mappedBy = "accountToId",cascade = CascadeType.ALL)
    public List<Transaction> transactionList1 = new ArrayList<>();
    @Column(name = "dailyTransactionLimit")
    private Integer dailyTransactionLimit = 3000;

    public Account(Long id, User user, String accountNumber,Integer balance) {
        this.id = id;
        this.user = user;
        this.Balance=balance;
        this.generateAccountnumber();
    }

    public Account(Long id, User user, String accountNumber, Integer balance, List<Card> cardList,List<Transaction> transactionList,List<Transaction> transactionList1 , Integer dailyTransactionLimit) {
        this.id = id;
        this.user = user;
        this.accountNumber = accountNumber;
        Balance = balance;
        this.cardList = cardList;
        this.transactionList=transactionList;
        this.transactionList1=transactionList1;
        this.dailyTransactionLimit = dailyTransactionLimit;
    }

    public Account() {
        this.generateAccountnumber();
    }
    private void generateAccountnumber(){
        Integer[] numbers = new Integer[16];
        for (int i = 0; i < numbers.length; i++) {
            Random random = new Random();
            numbers[i]= random.nextInt(10);
        }
        this.accountNumber = numbers[0].toString();
        for (int i = 1; i < numbers.length; i++) {
            this.accountNumber +=numbers[i].toString();
        }
    }
}
