package com.example.netbank.Entity;

import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.sql.Timestamp;


@Data
@Audited
@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "from_id")
    private Account accountFromId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "receiver_id")
    private Account accountToId;

    private int amount;
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    private Timestamp timestamp;
}
