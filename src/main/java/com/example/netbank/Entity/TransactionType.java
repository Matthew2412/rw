package com.example.netbank.Entity;

public enum TransactionType {
    UPLOAD(0), TRANSFER(0),TRANSACTIONAL_FEE(1),CREDIT_CARD_FEE(10),PAYMENT(0);
    public int amount;
    TransactionType(int amount){
        this.amount = amount;
    }
}
