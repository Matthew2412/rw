package com.example.netbank.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SignupResponse {
	private boolean mfa;
	private String secretImageUri;
}
