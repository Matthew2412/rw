package com.example.netbank.Entity;

import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Random;

@Entity
@Audited
@Data
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "CREDITCARD_NUMBER")
    private String creditCardNumber;
    @Column(name = "BALANCE")
    private int balance;
    @Column(name = "EXPIRATION_DATE")
    private Timestamp expirationDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;
    @Enumerated(EnumType.STRING)
    private CardType cardType = CardType.STANDARD;

    public Card() {
        Integer[] numbers = new Integer[16];
        for (int i = 0; i < numbers.length; i++) {
            Random random = new Random();
            numbers[i]= random.nextInt(10);
        }
        this.creditCardNumber = numbers[0].toString();
        for (int i = 1; i < numbers.length; i++) {
            this.creditCardNumber +=numbers[i].toString();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card )) return false;
        return id != null && id.equals(((Card) o).getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
