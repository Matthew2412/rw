package com.example.netbank.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class MfaLogIn {
	private String username;
	private String password;
	private String code;
}
