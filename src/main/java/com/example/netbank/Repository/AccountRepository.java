package com.example.netbank.Repository;

import com.example.netbank.Entity.Account;
import com.example.netbank.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {
    Account findAccountByAccountNumber(String accountNumber);
    List<Account> findByUser(User user);
}
