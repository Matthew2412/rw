package com.example.netbank.Repository;

import com.example.netbank.Entity.Account;
import com.example.netbank.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findByUserName(String userName);
    User findByEmail(String email);
    User findUserByAccount(Account accountNumber);
}
