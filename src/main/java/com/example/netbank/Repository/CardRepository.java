package com.example.netbank.Repository;

import com.example.netbank.Entity.Account;
import com.example.netbank.Entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {

    //visszaadja a felhasználó kártyáit
    public List<Card> findByAccount(Account account);

}
