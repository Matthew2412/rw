package com.example.netbank.Repository;

import com.example.netbank.Entity.Account;
import com.example.netbank.Entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransacitionRepository extends JpaRepository<Transaction,Long> {
    public List<Transaction> findByAccountFromId(Account account);
    public List<Transaction> findByAccountToId(Account account);
}
